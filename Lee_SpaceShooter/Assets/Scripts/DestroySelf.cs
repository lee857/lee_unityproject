﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySelf : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Invoke("DestroyMe", 1f) ;
	}

    void DestroyMe()
    {
        Destroy(gameObject);
    }
	
}
