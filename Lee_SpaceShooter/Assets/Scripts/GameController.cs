﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public GameObject[] hazards;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;
    public GameObject cola;

    public Text scoreText;
    public Text restartText;
    public Text gameOverText;
    public Text timeText;

    private PlayerMovement playerMovement;
    private bool gameOver;
    private bool restart;
    private int score;
    private float time;
    private float sec;

    void Start()
    {
        gameOver = false;
        restart = false;
        restartText.text = "";
        gameOverText.text = "";
        scoreText.material.color = Color.yellow;
        time = 30f;
        score = 0;
        UpdateScore();
        StartCoroutine (SpawnWaves());
    }

    void Update()
    {
        time -= Time.deltaTime;
        if (time > 0)
        {
            sec = (time % 60f);
            timeText.text =  "Time: " + sec.ToString("00") + " seconds";
        }
        if (time < 1)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }
    

    IEnumerator SpawnWaves ()
    {
        yield return new WaitForSeconds (startWait);
        while (true)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Vector3 spawnBarPosition = new Vector3(1.26f, spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                if (hazard.tag == "Bar")
                {
                    Instantiate(hazard, spawnBarPosition, spawnRotation);
                }
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(startWait);
            }
            Vector3 spawnLifePosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
            Quaternion spawnLifeRotation = Quaternion.identity;
            Instantiate(cola, spawnLifePosition, spawnLifeRotation);
            yield return new WaitForSeconds (waveWait);

            if (gameOver)
            {
                restartText.text = "Press 'R' for Restart";
                restart = true;
                break;
            }
        }
    }

    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }
    public void AddTime(int timeValue)
    {
        time += timeValue;
    }
    public void GameOver()
    {
        gameOverText.text = "Game Over!";
        gameOver = true;
    }
}