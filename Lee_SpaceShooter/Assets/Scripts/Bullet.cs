﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{


    public float speed;

    Rigidbody rb;

    private Transform playerTransform;
    Vector3 moveDirection;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        moveDirection = (playerTransform.transform.position - transform.position).normalized * speed;
        rb.velocity = new Vector3(moveDirection.x, 0f, moveDirection.z);
        Destroy(gameObject, 3f);
    }

    void onTriggerEnter (Collider col)
    {
        if (col.gameObject.name.Equals("Player"))
        {
            Debug.Log("Hit!");
            Destroy(gameObject);
        }
    }

}
