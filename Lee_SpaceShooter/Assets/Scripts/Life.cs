﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Life : MonoBehaviour
{
    public GameObject explosion;
    public int scoreValue;
    private GameController gameController;


    void OnTriggerEnter(Collider other)
    {
        
        if (other.tag == "Player")
        {
            other.GetComponent<PlayerMovement>().RestoreHealth(1);
            Destroy(gameObject);
        }
        if (other.tag == "Boundary" || other.tag == "Enemy")
        {
            return;
        }

        else
        {
            return;
        }
    }
}
