﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class PlayerMovement : MonoBehaviour {

    public float speed;
    public float tilt;
    public Boundary boundary;

    public GameObject shot;
    public Transform shotSpawn;
    public float fireRate;

    private float nextFire;
    
    int maxHealth = 5;
    public int health;

    public GameObject playerExplosion;
    private GameController gameController;

    public GameObject count1;
    public GameObject count2;
    public GameObject count3;

    private void Start()
    {
        health = maxHealth;
        count1.gameObject.SetActive(true);
        count2.gameObject.SetActive(true);
        count3.gameObject.SetActive(true);
        UpdateHealthBar();

        GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }

    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
            GetComponent<AudioSource>().Play();
        }
    }


    void FixedUpdate () {

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        GetComponent<Rigidbody>().velocity = movement * speed;

        GetComponent<Rigidbody>().position = new Vector3
        (
            Mathf.Clamp(GetComponent<Rigidbody>().position.x, boundary.xMin, boundary.xMax),
            0.0f,
            Mathf.Clamp(GetComponent<Rigidbody>().position.z, boundary.zMin, boundary.zMax)
        );
        GetComponent<Rigidbody>().rotation = Quaternion.Euler(0.0f, 0.0f, GetComponent<Rigidbody>().velocity.x * -tilt);
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        UpdateHealthBar();

        if (health <= 0)
        {
            Death();
        }
    }
    public void RestoreHealth(int restore)
    {
        health += restore;
        UpdateHealthBar();
        
    }

    public void Death()
    {
        Instantiate(playerExplosion, transform.position, transform.rotation);
        gameController.GameOver();
        Destroy(gameObject);
    }

    void UpdateHealthBar()
    {
        switch (health)
        {
            case 3:
                count1.gameObject.SetActive(true);
                count2.gameObject.SetActive(true);
                count3.gameObject.SetActive(true);
                break;
            case 2:
                count1.gameObject.SetActive(true);
                count2.gameObject.SetActive(true);
                count3.gameObject.SetActive(false);
                break;
            case 1:
                count1.gameObject.SetActive(true);
                count2.gameObject.SetActive(false);
                count3.gameObject.SetActive(false);
                break;
            case 0:
                count1.gameObject.SetActive(false);
                count2.gameObject.SetActive(false);
                count3.gameObject.SetActive(false);
                break;
        }
       
    }
}
